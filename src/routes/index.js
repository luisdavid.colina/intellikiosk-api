const { Router } = require('express');
const router = Router();

const { cashReceiver, receiverResponse } = require('../controllers/index.controller');

router.get('/cr/:id/:currency/:amount', cashReceiver);

router.get('/chk/', receiverResponse);

module.exports = router;

//receiver response