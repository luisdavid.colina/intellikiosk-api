const fs = require('fs')
const { spawn } = require('child_process');

const awaitResponse = () => {
  console.log('ok')
  return 1
}

const cashReceiver = async (req, res) => {
    const {id, currency, amount} = req.params;

    let text = id + '\n'+ currency + '\n'+  amount;
    
    await fs.writeFileSync('C:\\intellipos\\temp\\chk.txt', text);
    
    const bat = spawn('cmd.exe', ['/c', 'C:\\intellipos\\INYECTAR.bat']);

    bat.stdout.on('data', (data) => {
        console.log(data.toString());
      });
      
      bat.stderr.on('data', (data) => {
        console.error(data.toString());
      });
      
      bat.on('exit', (code) => {
        console.log(`Child exited with code ${code}`);
      });
    await setTimeout(awaitResponse, 10000)
    res.json('OK');
};

const receiverResponse = async (req, res) => {
  const {id, status} = req.query;
  res.json(id+' '+status);
};

module.exports = {
    cashReceiver, receiverResponse
};